#ifndef _HAL_H_
#define _HAL_H_

#include <stdio.h>

typedef unsigned char uint8_t;
typedef unsigned short int uint16_t;
typedef unsigned long int uint32_t;

typedef enum hal_file_io_error_code{
    hal_error_none,
    hal_can_not_open_file,
    hal_undefine_error,
} hal_file_io_error_code_t;


hal_file_io_error_code_t halFileInit(char * fileName);

hal_file_io_error_code_t halFileClose();

hal_file_io_error_code_t halFileReadSector(uint16_t inSectorId, \
                                            uint16_t inSectorSize, \
                                            uint8_t * outSectorData);

hal_file_io_error_code_t halFileReadMultiSector(uint16_t inFirstSectorId, \
                                                uint16_t inSectorSize, \
                                                uint8_t inNumberOfSector, \
                                                uint8_t * outMultiSectorData);

#endif

