#include <stdlib.h>
#include "fat12.h"

#define BYTES_PER_SECTOR_OFFSET 0x0B
#define SECTOR_PER_CLUSTER_OFFSET 0x0D
#define RESERVED_SECTOR_COUNT_OFFSET 0x0E
#define FAT_COUNT_OFFSET 0x10
#define MAX_ROOT_ENTRY_OFFSET 0x11
#define TOTAL_SECTOR_OFFSET 0x13
#define SECTOR_PER_FAT_OFFSET 0x16

#define FILE_SYSTEM_TYPE_OFFSET 0x39




/*
*    Boot sector Parameter
*/
static uint8_t _fat12BootSectorData[64];
static uint8_t _fat12SectorsPerCluster;
static uint8_t _fat12FATCount;
static uint16_t _fat12BytesPerSector;
static uint16_t _fat12ReservedSectorsCount;
static uint16_t _fat12RootDirMaxEntriesCount;
static uint16_t _fat12TotalSector;
static uint16_t _fat12SectorsPerFAT;

/*
*    FAT Details
*/
static uint16_t * _fat12FAT;

/*
*    Current sector and current Cluster Data
*    address storage address of current sector and cluster (like pointer)
*    data will be allocate in setup
*/
static uint16_t _fat12CurrentSectorIndex;
static uint16_t _fat12CurrentClusterIndex;

static uint8_t * _fat12CurrentSectorData;
static uint8_t * _fat12CurrentClusterData;

/*
*    Root Folder details
*/
static uint16_t _fat12RootDirectoryEntriesCount;
static uint16_t _fat12RootDirectoryRealEntriesCount;
static uint16_t _fat12RootDirectoryFileEntriesCount;
static uint16_t _fat12RootDirectorySubDirectoryEntriesCount;

static uint16_t _fat12RootDirectoryFirstSectorIndex;

/*
*    Current Folder details
*/
static uint16_t _fat12CurrentDirectoryEntriesCount;
static uint16_t _fat12CurrentDirectoryFirstClusterIndex;
static uint16_t _fat12ParentDirectoryFirstClusterIndex; // parent folder

/*
*    Current Entry details
*/
static uint8_t _fat12CurrentEntryShortName[8];

static uint8_t _fat12CurrentEntryExtension[4];

static uint8_t _fat12CurrentEntryModifiedHour;
static uint8_t _fat12CurrentEntryModifiedMinute;
static uint8_t _fat12CurrentEntryModifiedSecond;

static uint8_t _fat12CurrentEntryModifiedDate;
static uint8_t _fat12CurrentEntryModifiedMonth;
static uint8_t _fat12CurrentEntryModifiedYear;

static uint8_t _fat12CurrentEntrySize;


/*************************************************************************************************************
*    local function
**************************************************************************************************************/

/*
*    read boot parameter
*/
bool _fat12ReadBootSector();

/*
*    check file system is fat12
*/
bool _fat12fileSystemIsFat12();

bool _fat12Initial();

/*
*    read FAT, allocate and storage data to buffer uint16_t * _fat12FAT 
*/
bool _fat12ReadFAT();

/*
*    find number of entry,
*    check each 32 bytes in first sector of Root Directory
*    jump to next 32 bytes in sector
*    jump to next sector (root folder storage in multi sectors)
*    loop
*    stop when find 32 byte 0x00
*/
bool _fat12ReadRootDirectory();

/*
*    find number of entry,
*    check each 32 bytes in first sector of first cluster of Sub Directory
*    jump to next 32 bytes in sector
*    jump to next sector
*    jump to next cluster (sub folder storage in multi clusters)
*    loop
*    stop when find 32 byte 0x00
*/
bool _fat12ReadCurrentDirectory();

/*
*    load data of cluster and next cluster index to buffer
*/

bool _fat12ReadCluster(uint16_t clusterIndex);
bool _fat12GetNextCluster(uint16_t inClusterIndex, uint16_t * OutNextClusterIndex);



uint16_t _fat12ConvertEndian2Byte(uint8_t inBuff[2]);
uint32_t _fat12ConvertEndian4Byte(uint8_t inBuff[4]);
void _fat12ConvertEndian3Byte(uint8_t inBuff[3], uint16_t * outValue1, uint16_t * outValue2);


/*************************************************************************************************************
*    local function declare 
**************************************************************************************************************/

bool _fat12ReadBootSector(){
    bool returnVal = 0;
    if(!halFileReadSector(0, 64, _fat12BootSectorData)){
        returnVal = 1;
        _fat12BytesPerSector = _fat12ConvertEndian2Byte(_fat12BootSectorData + BYTES_PER_SECTOR_OFFSET);
        _fat12SectorsPerCluster = *(_fat12BootSectorData + SECTOR_PER_CLUSTER_OFFSET);
        _fat12ReservedSectorsCount = _fat12ConvertEndian2Byte(_fat12BootSectorData + RESERVED_SECTOR_COUNT_OFFSET);
        _fat12FATCount = *(_fat12BootSectorData + FAT_COUNT_OFFSET);
        _fat12RootDirMaxEntriesCount = _fat12ConvertEndian2Byte(_fat12BootSectorData + MAX_ROOT_ENTRY_OFFSET);
        _fat12TotalSector = _fat12ConvertEndian2Byte(_fat12BootSectorData + TOTAL_SECTOR_OFFSET);
        _fat12SectorsPerFAT = _fat12ConvertEndian2Byte(_fat12BootSectorData + SECTOR_PER_FAT_OFFSET);
    }
    
    return returnVal;
}

bool _fat12fileSystemIsFat12(){
    bool returnVal = 0;
    
    if((*(_fat12BootSectorData + FILE_SYSTEM_TYPE_OFFSET) == '1') && (*(_fat12BootSectorData + FILE_SYSTEM_TYPE_OFFSET + 1) == '2')) returnVal = 1;
    
    return returnVal;
}

bool _fat12Initial(){
    bool returnVal = 1;
    
    //fat data 12bit/cluster -> uint16_t
    _fat12FAT = (uint16_t*) malloc(((_fat12SectorsPerFAT * _fat12BytesPerSector) * 3 / 2) * sizeof(uint16_t));
    
    //sector data using uint8_t
    _fat12CurrentSectorData = (uint8_t*) malloc(_fat12BytesPerSector * sizeof(uint8_t));
    
    //cluster data using uint8_t
    _fat12CurrentClusterData = (uint8_t*) malloc((_fat12SectorsPerCluster * _fat12BytesPerSector) * sizeof(uint8_t));
    
    return returnVal;
}

bool _fat12ReadFAT(){
    return 1;
}

bool _fat12ReadRootDirectory(){
    _fat12RootDirectoryFirstSectorIndex = _fat12ReservedSectorsCount + (_fat12FATCount * _fat12SectorsPerFAT);
    halFileReadSector(_fat12RootDirectoryFirstSectorIndex, _fat12BytesPerSector, _fat12CurrentSectorData);
    
    uint8_t checkBuffer[32];
    
    _fat12RootDirectoryEntriesCount = 0;
    _fat12RootDirectoryRealEntriesCount = 0;
    bool breakWhile = 0;
    while(!breakWhile){
        uint16_t sum = 0;
        uint8_t runCount32 = 0;
        for(runCount32; runCount32 < 32; runCount32 ++){
            checkBuffer[runCount32] = *(_fat12CurrentSectorData + (_fat12RootDirectoryEntriesCount * 32) + runCount32);
            printf("%c",checkBuffer[runCount32]);
            sum += checkBuffer[runCount32];
        }
        if(sum != 0){
            _fat12RootDirectoryEntriesCount++;
            if(checkBuffer[11] == 0x10){
                _fat12RootDirectorySubDirectoryEntriesCount++;
            }
            else if(checkBuffer[11] != 0x0F){
                _fat12RootDirectoryFileEntriesCount++;
            }
        }
        else{
            breakWhile = true;
        }
    }
}

bool _fat12ReadCurrentDirectory();
bool _fat12ReadCurrentFile();

bool _fat12ReadCluster(uint16_t clusterIndex);
bool _fat12GetNextCluster(uint16_t inClusterIndex, uint16_t * OutNextClusterIndex);

uint16_t _fat12ConvertEndian2Byte(uint8_t inBuff[2]){
    uint16_t returnVal;
    returnVal = (*(inBuff + 1)) * 256 + (*(inBuff));
    return returnVal;
}

uint32_t _fat12ConvertEndian4Byte(uint8_t inBuff[4]);
void _fat12ConvertEndian3Byte(uint8_t inBuff[3], uint16_t * outValue1, uint16_t * outValue2);


/*************************************************************************************************************
*    API function declare 
**************************************************************************************************************/

fat12_fsec_t fat12LoadFileImage(char * fileName){
    fat12_fsec_t returnVal = fat12_fsec_none;
    
    //load file
    if(halFileInit(fileName)){
        returnVal = fat12_fsec_can_not_open_file;
    }
    else{
        //reder boot parameter
        if(!_fat12ReadBootSector()){
            returnVal = fat12_fsec_can_not_read_file;
        }
        
        //check file system
        if(!_fat12fileSystemIsFat12()){
            returnVal = fat12_fsec_type_is_not_fat12;
        }
        
        //alocate and initial buffer
        if(!_fat12Initial()){
            returnVal = fat12_fsec_undefine_error;
        }
        
        //read FAT
        if(!_fat12ReadFAT()){
            returnVal = fat12_fsec_undefine_error;
        }
        
        //read Root Directory
        if(!_fat12ReadRootDirectory()){
            returnVal = fat12_fsec_undefine_error;
        }
        
        printf("alo");
    }
    
    return returnVal;
}

fat12_fsec_t fat12CloseFileImage();

bool fat12GetRootDirectoryEntriesCount(uint16_t * outSubDirectoryCount, uint16_t * outFileCount){
    bool returnVal = 1;
    
    *outSubDirectoryCount = _fat12RootDirectorySubDirectoryEntriesCount;
    *outFileCount = _fat12RootDirectoryFileEntriesCount;
    
    return returnVal;
}
fat12_esc_t fat12GetRootDirectoryEntryDisplay(uint16_t inEntryIndex,\
                                                char ** outName,\
                                                char ** outExtension,\
                                                char ** outType,\
                                                char ** outTimeModified,\
                                                char ** outSize);

bool fat12OpenFolder(uint16_t inEntryIndex);

bool fat12GetCurrentDirectoryEntriesCount(uint16_t * outCount);
fat12_esc_t fat12GetCurrentDirectoryEntryDisplay(uint16_t inEntryIndex,\
                                                char ** outName,\
                                                char ** outExtension,\
                                                char ** outType,\
                                                char ** outTimeModified,\
                                                char ** outSize);

bool fat12OpenFile(uint16_t inEntryIndex, uint8_t * outEOF, char ** outCluster);

