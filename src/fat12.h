#ifndef _FAT12_H_
#define _FAT12_H_

#include <stdbool.h>
#include "hal.h"

typedef enum fat12_file_system_error_code{
    fat12_fsec_none,
    fat12_fsec_can_not_open_file,
    fat12_fsec_can_not_read_file,
    fat12_fsec_type_is_not_fat12,
    fat12_fsec_undefine_error
} fat12_fsec_t;

typedef enum fat12_entry_status_code{
    fat12_eec_none,
    fat12_eec_entry_is_short_file,
    fat12_eec_entry_is_empty_file,
    fat12_eec_entry_is_sub_folder,
} fat12_esc_t;


/*
*    load and parse image file
*/
fat12_fsec_t fat12LoadFileImage(char * fileName);

/*
*    close file
*/
fat12_fsec_t fat12CloseFileImage();

/*
*    get display infomation of each entry in root folder
*/
bool fat12GetRootDirectoryEntriesCount(uint16_t * outSubDirectoryCount, uint16_t * outFileCount);
fat12_esc_t fat12GetRootDirectoryEntryDisplay(uint16_t inEntryIndex,\
                                                char ** outName,\
                                                char ** outExtension,\
                                                char ** outType,\
                                                char ** outTimeModified,\
                                                char ** outSize);

/*
*    jump current pointer to sub folder
*/
bool fat12OpenFolder(uint16_t inEntryIndex);

/*
*    get display infomation of each entry in sub folder
*/
bool fat12GetCurrentDirectoryEntriesCount(uint16_t * outCount);
fat12_esc_t fat12GetCurrentDirectoryEntryDisplay(uint16_t inEntryIndex,\
                                                char ** outName,\
                                                char ** outExtension,\
                                                char ** outType,\
                                                char ** outTimeModified,\
                                                char ** outSize);

/*
*    get display data of file in current folder
*    return cluster and jump to next cluster, outEOF = 1 if cluster 
*/
bool fat12OpenFile(uint16_t inEntryIndex, uint8_t * outEOF, char ** outCluster);

#endif

