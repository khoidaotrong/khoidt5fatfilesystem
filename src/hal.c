#include "hal.h"

static FILE * _halFile;

hal_file_io_error_code_t halFileInit(char * fileName){
    hal_file_io_error_code_t returnVal = hal_can_not_open_file;
    
    _halFile = fopen(fileName,"rb");
    if(_halFile != NULL) returnVal = hal_error_none;
    
    return returnVal;
}

hal_file_io_error_code_t halFileClose(){
    hal_file_io_error_code_t returnVal = hal_error_none;
    
    fclose(_halFile);
    if(_halFile != NULL) returnVal = hal_undefine_error;
    
    return returnVal;
}

hal_file_io_error_code_t halFileReadSector(uint16_t inSectorId, \
                                            uint16_t inSectorSize, \
                                            uint8_t * outSectorData){
    hal_file_io_error_code_t returnVal = hal_error_none;
    
    fseek(_halFile, (inSectorId * inSectorSize), SEEK_SET);
    
    size_t check = fread(outSectorData, 1, inSectorSize, _halFile);
    if(check != inSectorSize) returnVal = hal_undefine_error;
    
    return returnVal;
}

hal_file_io_error_code_t halFileReadMultiSector(uint16_t inFirstSectorId, \
                                                uint16_t inSectorSize, \
                                                uint8_t inNumberOfSector, \
                                                uint8_t * outMultiSectorData){
    hal_file_io_error_code_t returnVal = hal_error_none;
    
    fseek(_halFile, (inFirstSectorId * inSectorSize), SEEK_SET);
    
    size_t check = fread(outMultiSectorData, 1, inSectorSize * inNumberOfSector, _halFile);
    if(check != inSectorSize * inNumberOfSector) returnVal = hal_undefine_error;
    
    return returnVal;
}

