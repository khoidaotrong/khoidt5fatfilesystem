#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "fat12.h"


int main(int argc, char *argv[]) {
    
    if(!fat12LoadFileImage(argv[1])){
        printf("\nkhong tim thay file");
    }
    else{
        
        char name[10];
        char extension[10];
        char type[4];
        char TimeModified[16];
        char size[8];
        
        char displayFrame[50];
        
        
        uint16_t fileEntryCount = 0;
        uint16_t subFolderEntryCount = 0;
        
        sprintf(name,"Name");
        sprintf(extension, "Extension");
        sprintf(type, "Type");
        sprintf(TimeModified, "Time");
        sprintf(size, "Size");
        
        sprintf(displayFrame,"\nNo.\t|%s\t\t|%s\t\t|%s\t\t|%s\t\t|%s",name, extension, type, TimeModified, size);
        
        printf("\nRoot Directory: ============================================================");
        printf(displayFrame);
        
        fat12GetRootDirectoryEntriesCount(&subFolderEntryCount, &fileEntryCount);
        
        uint16_t run = 0;
        for(run = 0; run<subFolderEntryCount; run++){
            sprintf(displayFrame,"\n%d\t|%s\t\t|%s\t\t|%s\t\t|%s\t\t|%s",run+1, name, extension, "Dir", TimeModified, size);
            printf(displayFrame);
        }
        for(run = 0; run<fileEntryCount; run++){
            sprintf(displayFrame,"\n%d\t|%s\t\t|%s\t\t|%s\t\t|%s\t\t|%s",run+1, name, extension, "File", TimeModified, size);
            printf(displayFrame);
        }
    }
    
    return 0;
}


